# SRU-Parser - Wienbibliothek Plakatsammlung

* [This parser](wbr_SRU.ipynb) fetches all bibliographic records in the Wienbibliothek library catalog which are marked in the `local_field_982=Filmplakat`. 
* The result is originally given in MARCXML. For easier and faster re-use of the metadata, a minimum set of fields where parsed and transformed into a JSON object
  * Information about persons with an ID to the Gemeinsame Normdatei was enriched with a link to Wikipedia, using a lookup on https://lobid.org/gnd (done in function `getWikiUrl(gnd)`)

**Parsed values**
* AC-Nummer [Marc 009] (bibliographic record ID)
* Title [Marc 245]3
* Image-URL [Marc 856\$\$"Image"]
* Depicted Persons [Marc 700\$\$ e"Abgebilde Person"]
  * plus GND-Id if given
* Creator [Marc 100]
  * plus GND-Id if given
* publicationDate [Marc 264]
* description Text [Marc 505]
* Title of depicted film [Marc 240]

**JSON-Result [demo]**
```json
[
  {
    "acnr": "AC10543997", 
    "title": "!!Herrrrrreinspaziert!! ... Nur hier sehen Sie die garantiert echt orientalische Baucht\u00e4nzerin Salome in ihren verf\u00fchrerischen Liebest\u00e4nzen",
    "imgUrl": "http://media.obvsg.at/AC10543997-4201",
    "depicts": [{ "name": "Birgel, Willy", 
                  "gnd": "(DE-588)116189754", 
                  "wikiUrl": "https://de.wikipedia.org/wiki/Willy_Birgel"}],
     "creators": [{"name": "Pewas, Peter"}],
     "publicationDate": "[ca. um 1930]",
     "description": "Schriftplakat"
  }
]
```
